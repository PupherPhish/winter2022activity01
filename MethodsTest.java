public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		
		methodTwoInputNoReturn(x, 3.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double Result = sumSquareRoot(6, 3);
		System.out.println(Result);
		
		String s1 = "hello";
		String s2 = "goodbye";
		s1.length();
		s2.length();
		System.out.println(s1.length() + " " + s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int Number){
		System.out.println("Inside the method one input no return");
		System.out.println(Number);
	}
	public static void methodTwoInputNoReturn(int Inty, double Doubly){
		System.out.println(Inty + " " + Doubly);
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int Num1, int Num2){
		int Num3 = Num1 + Num2;
		double Root = Math.sqrt(Num3);
		return Root;
	}
}